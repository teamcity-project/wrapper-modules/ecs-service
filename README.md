# ECS-service wrapper module for Teamcity project

This module is wrapper for [terraform-aws-ecs by HENNGE](https://github.com/HENNGE/terraform-aws-ecs) module
and uses it as solution example

The module creates:
  - ECS Task Definition
  - ECS Service
  - CloudWatch Logs group for collect container logs (optional)
  - Custom ECS execution role if using the secret environment variables stored in SSM

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.42 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| alb\_target\_group\_arns | ARNs of ALB target group | `list(string)` | `[]` | no |
| awslogs\_group | CloudWatch AWS logs group. If defined, logs group will be created | `string` | `""` | no |
| awslogs\_region | CloudWatch AWS logs region | `string` | `""` | no |
| awslogs\_retention\_in\_days | Specifies the number of days you want to retain log events in the specified log group | `number` | `31` | no |
| awslogs\_stream\_prefix | CloudWatch AWS logs stream prefix | `string` | `""` | no |
| bind\_mount\_volumes | List of maps with container volumes, host mount points, and container mount points | `list(map(string))` | `[]` | no |
| capacity\_provider\_arn | Run the service only on this capacity provider | `string` | n/a | yes |
| container\_command | Container command | `list(string)` | `[]` | no |
| container\_cpu | Container CPU limit consumption (1024 is 1 CPU) | `number` | n/a | yes |
| container\_entrypoint | Container command | `list(string)` | `[]` | no |
| container\_env | Container environment variables | `list(map(string))` | `[]` | no |
| container\_image | Container image name | `string` | n/a | yes |
| container\_memory | Container memory limit consumption (in MiB) | `number` | n/a | yes |
| container\_port | The port on the container to associate with the load balancer | `number` | n/a | yes |
| container\_secret\_env | List of maps with environment variables ("name" field) and ARN of SSM parameters or Secret Manager secrets ("valueFrom" field) | `list(map(string))` | `[]` | no |
| container\_user | The user name to use inside the container. | `string` | n/a | yes |
| container\_version | Container image version | `string` | n/a | yes |
| deployment\_maximum\_percent | upper limit (% of `desired_count`) of # of running tasks during a deployment. Do not fill when using `DAEMON` scheduling strategy. | `number` | n/a | yes |
| deployment\_minimum\_healthy\_percent | lower limit (% of `desired_count`) of # of running tasks during a deployment | `number` | `100` | no |
| desired\_count | Desired number of running containers | `number` | `1` | no |
| ecs\_cluster | ECS cluster | `string` | n/a | yes |
| health\_check\_grace\_period\_seconds | Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown, up to 2147483647. Only valid for services configured to use load balancers. | `number` | n/a | yes |
| host\_port | Host port associated with container port | `number` | n/a | yes |
| iam\_daemon\_role | The custom IAM Role to assign for the ECS container agent and Docker daemon. If not defined, the role will be created with standard settings | `string` | n/a | yes |
| iam\_task\_role | IAM Role for task to use to access AWS services | `string` | n/a | yes |
| ignore\_desired\_count\_changes | Ignores any changes to `desired_count` parameter after apply. Note updating this value will destroy the existing service and recreate it | `bool` | `false` | no |
| network\_mode | Container network mode | `string` | `"bridge"` | no |
| protocol | Network protocol type (tcp/udp) | `string` | `"tcp"` | no |
| region | AWS region | `string` | n/a | yes |
| service\_name | ECS service name | `string` | n/a | yes |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |

## Outputs

No output.

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
