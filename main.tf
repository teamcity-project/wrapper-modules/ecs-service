terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12"

  required_providers {
    aws = "~> 2.42"
  }
}

provider "aws" {
  region = var.region
}

# Split the bind_mount_volumes list of maps to 2 list of maps: for container defintion and for task definition

locals {
  mount_points = [for item in var.bind_mount_volumes :
  { for k, v in item : replace(k, "name", "sourceVolume") => v if k != "host_path" }]

  volume_conf = [for item in var.bind_mount_volumes :
  { for k, v in item : k => v if k != "containerPath" }]

  create_custom_execution_role = length(var.container_secret_env) != 0 ? true : false
}

module "ecs_service" {
  source = "github.com/HENNGE/terraform-aws-ecs.git//modules/simple/ec2/?ref=1.0.0"

  name                         = "${var.service_name}-svc"
  cluster                      = var.ecs_cluster
  cpu                          = var.container_cpu
  memory                       = var.container_memory
  desired_count                = var.desired_count
  ignore_desired_count_changes = var.ignore_desired_count_changes
  network_mode                 = var.network_mode

  capacity_provider_arn = var.capacity_provider_arn

  # Workaround when destroy fails
  target_group_arn = length(var.alb_target_group_arns) == 0 ? null : var.alb_target_group_arns[0]

  # container_name must be the same with the name defined in container_definitions!
  container_name = "${var.service_name}-cont"

  # Exposed container port, same as in `containerPort` in container_definitions
  container_port = var.container_port

  deployment_minimum_healthy_percent = var.deployment_minimum_healthy_percent
  deployment_maximum_percent         = var.deployment_maximum_percent
  health_check_grace_period_seconds  = var.health_check_grace_period_seconds

  volume_configurations = local.volume_conf

  iam_task_role = var.iam_task_role

  # Always override creation the custom ECS execution role, if the external ARN is specified
  iam_daemon_role = (local.create_custom_execution_role && var.iam_daemon_role == null) ? aws_iam_role.ecs_execution_role[0].arn : var.iam_daemon_role

  # We don't use the ECS Preview Support for EFS file systems as it has not a production-ready status yet.
  container_definitions = templatefile("${path.module}/templates/container_definitions.tpl", {
    name                  = "${var.service_name}-cont"
    cpu                   = var.container_cpu
    memory                = var.container_memory
    image                 = var.container_image
    version               = var.container_version
    port                  = var.container_port
    host_port             = var.host_port
    protocol              = jsonencode(var.protocol)
    env                   = jsonencode(var.container_env)
    secret_env            = jsonencode(var.container_secret_env)
    command               = jsonencode(var.container_command)
    entrypoint            = jsonencode(var.container_entrypoint)
    awslogs_region        = var.awslogs_region
    awslogs_group         = var.awslogs_group
    awslogs_stream_prefix = var.awslogs_stream_prefix
    target_group_arn_len  = length(var.alb_target_group_arns)
    mount_points          = jsonencode(local.mount_points)
    user                  = jsonencode(var.container_user)
  })

  //tags = var.tags
}

resource "aws_cloudwatch_log_group" "ecs_log_group" {
  count = var.awslogs_group != "" ? 1 : 0

  name              = var.awslogs_group
  retention_in_days = var.awslogs_retention_in_days
  tags              = var.tags
}


# TODO: Move the resources to the separate module
# ==== Custom ECS execution role ======

data "aws_iam_policy_document" "assume_ecs_task_role_policy" {
  count = local.create_custom_execution_role ? 1 : 0

  version = "2012-10-17"

  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "allow_get_ssm_params_policy" {
  count = local.create_custom_execution_role ? 1 : 0

  version = "2012-10-17"

  statement {
    effect = "Allow"

    actions = [
      "ssm:GetParameters",
      "secretsmanager:GetSecretValue",
      "kms:Decrypt"
    ]

    # Get ARN list of SSM parameters
    resources = [for item in var.container_secret_env : lookup(item, "valueFrom")]
  }
}

resource "aws_iam_role" "ecs_execution_role" {
  count = local.create_custom_execution_role ? 1 : 0

  name               = "${var.service_name}-ecs-task-execution-role"
  assume_role_policy = data.aws_iam_policy_document.assume_ecs_task_role_policy[0].json

  tags = var.tags
}

resource "aws_iam_policy" "ecs_get_ssm_params_policy" {
  count = local.create_custom_execution_role ? 1 : 0

  name   = "${var.service_name}_ecs_allow_get_ssm_params_policy"
  policy = data.aws_iam_policy_document.allow_get_ssm_params_policy[0].json

}

resource "aws_iam_role_policy_attachment" "ecs_execution_role_cloudwatch" {
  count = local.create_custom_execution_role ? 1 : 0

  role       = aws_iam_role.ecs_execution_role[0].id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role_policy_attachment" "ecs_execution_role_get_ssm_params" {
  count = local.create_custom_execution_role ? 1 : 0

  role       = aws_iam_role.ecs_execution_role[0].id
  policy_arn = aws_iam_policy.ecs_get_ssm_params_policy[0].arn
}
