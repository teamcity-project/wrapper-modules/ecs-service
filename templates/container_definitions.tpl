[
  {
    "name": "${name}",
    "image": "${image}:${version}",
    "cpu": ${cpu},
    "memory": ${memory},
    "memoryReservation": ${memory},
    "essential": true,
    "portMappings": [%{ if target_group_arn_len != 0 }
      {
        "containerPort": ${port}%{ if host_port != null },
        "hostPort": ${host_port},
        "protocol": ${protocol}%{ endif }
      }%{ endif }
    ],
    "logConfiguration": {%{ if awslogs_group != "" }
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "${awslogs_region}",
        "awslogs-group": "${awslogs_group}",
        "awslogs-stream-prefix": "${awslogs_stream_prefix}"
      }%{ endif }
    },
    %{ if user != "\"\"" }"user": ${user},%{ endif }
    "environment": ${env},
    "secrets": ${secret_env},
    "command": ${command},
    "entrypoint": ${entrypoint}%{ if mount_points != []},
    "mountPoints": ${mount_points}%{ endif }
  }
]
