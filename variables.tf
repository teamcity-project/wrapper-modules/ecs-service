variable "region" {
  description = "AWS region"
  type        = string
}

variable "service_name" {
  description = "ECS service name"
  type        = string
}

variable "ecs_cluster" {
  description = "ECS cluster"
  type        = string
}

variable "network_mode" {
  description = "Container network mode"
  type        = string
  default     = "bridge"
}

variable "container_cpu" {
  description = "Container CPU limit consumption (1024 is 1 CPU)"
  type        = number
}

variable "container_memory" {
  description = "Container memory limit consumption (in MiB)"
  type        = number
}

variable "container_image" {
  description = "Container image name"
  type        = string
}

variable "container_version" {
  description = "Container image version"
  type        = string
}

variable "container_port" {
  description = "The port on the container to associate with the load balancer"
  type        = number
  default     = null
}

variable "host_port" {
  description = "Host port associated with container port"
  type        = number
  default     = null
}

variable "protocol" {
  description = "Network protocol type (tcp/udp)"
  type        = string
  default     = "tcp"
}

variable "container_env" {
  description = "Container environment variables"
  type        = list(map(string))
  default     = []
}

variable "container_secret_env" {
  description = "List of maps with environment variables (\"name\" field) and ARN of SSM parameters or Secret Manager secrets (\"valueFrom\" field)"
  type        = list(map(string))
  default     = []
}

variable "container_command" {
  description = "Container command"
  type        = list(string)
  default     = []
}

variable "container_entrypoint" {
  description = "Container command"
  type        = list(string)
  default     = []
}

variable "container_user" {
  description = "The user name to use inside the container."
  type        = string
  default     = ""
}

variable "awslogs_region" {
  description = "CloudWatch AWS logs region"
  type        = string
  default     = ""
}

variable "awslogs_group" {
  description = "CloudWatch AWS logs group. If defined, logs group will be created"
  type        = string
  default     = ""
}

variable "awslogs_stream_prefix" {
  description = "CloudWatch AWS logs stream prefix"
  type        = string
  default     = ""
}

variable "awslogs_retention_in_days" {
  description = "Specifies the number of days you want to retain log events in the specified log group"
  type        = number
  default     = 31
}

variable "desired_count" {
  description = "Desired number of running containers"
  type        = number
  default     = 1
}

variable "ignore_desired_count_changes" {
  description = "Ignores any changes to `desired_count` parameter after apply. Note updating this value will destroy the existing service and recreate it"
  type        = bool
  default     = false
}

variable "alb_target_group_arns" {
  description = "ARNs of ALB target group"
  type        = list(string)
  default     = []
}

variable "health_check_grace_period_seconds" {
  description = "Seconds to ignore failing load balancer health checks on newly instantiated tasks to prevent premature shutdown, up to 2147483647. Only valid for services configured to use load balancers."
  default     = null
  type        = number
}

variable "deployment_minimum_healthy_percent" {
  description = "lower limit (% of `desired_count`) of # of running tasks during a deployment"
  default     = 100
  type        = number
}

variable "deployment_maximum_percent" {
  description = "upper limit (% of `desired_count`) of # of running tasks during a deployment. Do not fill when using `DAEMON` scheduling strategy."
  default     = null
  type        = number
}

variable "bind_mount_volumes" {
  description = "List of maps with container volumes, host mount points, and container mount points"
  default     = []
  type        = list(map(string))
}

variable "capacity_provider_arn" {
  description = "Run the service only on this capacity provider"
  default     = null
  type        = string
}

variable "iam_task_role" {
  description = "IAM Role for task to use to access AWS services"
  default     = null
  type        = string
}

variable "iam_daemon_role" {
  description = "The custom IAM Role to assign for the ECS container agent and Docker daemon. If not defined, the role will be created with standard settings"
  default     = null
  type        = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}
